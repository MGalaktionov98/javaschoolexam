package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * this is class PyramidBuilder with method for building
 * a pyramid from array
 */
public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if (inputNumbers.size() > Character.MAX_VALUE) throw new CannotBuildPyramidException();
        else {
            boolean buildingFlag;
            boolean nullPresenceFlag = false;
            int[][] matrix;//Получившаяся матрица

            System.out.println("Введенная последовательность: " + inputNumbers);
            //Check the array size
            int size = inputNumbers.size();
            //Check if it is possible to build a pyramid from this amount of elements
            int count = 0;
            int rows = 1;
            int cols = 1;
            while (count < size) {
                count = count + rows;
                rows++;
                cols = cols + 2;
            }
            rows = rows - 1;//Actual amount of rows
            cols = cols - 2;//Actual amount of cols

            if (size == count) {
                buildingFlag = true;
            } else buildingFlag = false;
            for (int i = 0; i < inputNumbers.size(); i++) {
                if (inputNumbers.get(i) == null) {
                    nullPresenceFlag = true;
                }
            }
            if (buildingFlag && !nullPresenceFlag) {
                List<Integer> sortedInputnumbers = inputNumbers.stream().sorted().collect(Collectors.toList());

                System.out.println("Отсортированная последовательность по возрастанию: " + sortedInputnumbers);
                System.out.println("Количество элементов в последовательности: " + size);
                System.out.println("Число столбцов(cols) матрицы будет равно: " + cols);
                System.out.println("Число строк(rows) матрицы будет равно: " + rows);

                //Initialize matrix with zeroes
                matrix = new int[rows][cols];
                for (int[] row : matrix) {
                    Arrays.fill(row, 0);
                }

                //Building pyramid
                int center = (cols / 2);//Central pos
                count = 1;
                int arrIdx = 0;

                for (int i = 0, offset = 0; i < rows; i++, offset++, count++) {
                    int start = center - offset;
                    for (int j = 0; j < count * 2; j += 2, arrIdx++) {
                        matrix[i][start + j] = sortedInputnumbers.get(arrIdx);
                    }
                }

                /******** ВЫВОДИМ МАТРИЦУ НА ЭКРАН *******/
                for (int[] a : matrix)//выводим матрицу на экран
                {
                    for (int b : a)
                        System.out.print(b + " ");
                    System.out.println();
                }
            }//Выбрасываем исключение
            else {
                throw new CannotBuildPyramidException();
            }
            return matrix;
        }
    }

}
