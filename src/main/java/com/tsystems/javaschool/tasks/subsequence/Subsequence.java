package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

/**
 * this is class Subsequence with method for
 * checking if it is possible a sequence which is equal to the first
 * one by removing some elements from the second one.
 */
public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if ( x == null || y == null) {
            throw new IllegalArgumentException();
        }else{
            //  Write down sequences
            printSequences(x,y);
            /*  Check if there any matches in bouth sequences,
                if element from second sequence won't match any
                element from 1-st it will be removed this is the
                fist stage of testing
            */
            boolean matchFlag1;
            for (int i = 0; i < y.size(); i++) {
                matchFlag1 = false;
                for (int j = 0; j < x.size(); j++) {
                    if (y.get(i)==(x.get(j))) {
                        matchFlag1 = true;
                    }
                }
                if (matchFlag1 == false) {
                    y.remove(i);
                }
            }
            //  Write down sequences again
            System.out.println("\n" + "Sequences after removing elemnts from second sequence");
            printSequences(x,y);
            /*  this is the second stage of testing it will remove
                repeating simbols witch match with simbols from first
                sequence if it is necessary
            */
            int delta = y.size()-x.size();
            System.out.println("length difference = "+ delta);
            if(delta >= 0) {
                for (int i = 0; i < x.size()-1; i++) {
                    if (x.get(i) != y.get(i) && x.get(i) == y.get(i + delta)) {
                        y.remove(i);
                    }
                }
            }
            System.out.println("Sequences after second stage of testing ");
            printSequences(x,y);
            System.out.println("\n"+"\n"+"\n");
            // TODO: Implement the logic here
            boolean flag = false;
            if(x.equals(y) || x.isEmpty())
                flag = true;
            return flag;
        }
    }

    public void printSequences(List x, List y){
        System.out.println("\n" + "X:");
        for (int i = 0; i < x.size(); i++) {
            System.out.print(x.get(i) + " ");
        }
        System.out.println("\n" + "Y:");
        for (int i = 0; i < y.size(); i++) {
            System.out.print(y.get(i) + " ");
        }

    }
}
