package com.tsystems.javaschool.tasks;

import com.tsystems.javaschool.tasks.pyramid.PyramidBuilder;
import com.tsystems.javaschool.tasks.subsequence.Subsequence;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
    public static void main( String[] args)
    {
        // just check that everything works
        PyramidBuilder pyramidBuilder = new PyramidBuilder();
        pyramidBuilder.buildPyramid(Arrays.asList(1,2,3));

        Subsequence subsequence = new Subsequence();
        List x = Stream.of(1,2,3,4,5,6,7).collect(Collectors.toList());
        List y = Stream.of(14,1,2,5,3,4,5,10,6,7,11).collect(Collectors.toList());
        subsequence.find(x,y);
    }
}
